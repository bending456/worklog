from __future__ import print_function
import datetime
import sys
import os
import os.path as osp

##editor="/usr/bin/geany"
##editor="geany"
##editor="emacsclient -n -c"
##editor="atom"
editor="code"
extension = ".md"

sepline = "-"*80
initbody="""
​
----------------------------------------------------------
[Note]
"""

def makefile(date):
    datestr = date.strftime("%Y-%m-%d")
    yearstr = datestr[0:4]
    datehdr = date.strftime("%a %d-%b-%Y")
    if not osp.isdir(yearstr):
        os.mkdir(yearstr)
    fname = osp.join(yearstr,datestr + extension)
    data = "%s\n%s\n%s"%(datehdr,sepline,initbody)
    if not osp.isfile(fname):
      fp=open(fname,'w')
      fp.write(data)
      fp.close()
    return fname

def yesterday():
    today=datetime.date.today()
    day = datetime.timedelta(days=1)
    yester = today - day
    return yester

if len(sys.argv) > 1:
    if sys.argv[1] == "yesterday":
        fname=makefile(yesterday())
    elif sys.argv[1] == "today":
        fname=makefile(datetime.date.today())
    fullpath = osp.abspath(fname)
    cmdstr = editor + " "+fullpath
    print(cmdstr)
    os.system(cmdstr)
